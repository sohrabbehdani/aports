# Contributor: Krassy Boykinov <kboykinov@teamcentrixx.com>
# Maintainer: Krystian Chachuła <krystian@krystianch.com>
pkgname=plantuml
pkgver=1.2024.4
pkgrel=0
pkgdesc="Draw UML diagrams, using a simple and human readable text description"
url="https://plantuml.com/"
# aarch64: gradle segfaults jvm: https://github.com/plantuml/plantuml/issues/1543
# riscv64: blocked by java-jre
# ppc64le: build times out
arch="noarch !aarch64 !riscv64 !ppc64le"
license="GPL-3.0-or-later"
depends="
	busybox
	graphviz
	gtk+2.0
	java-common
	java-jre
	"
makedepends="
	font-dejavu
	java-jdk
	"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/plantuml/plantuml/archive/refs/tags/v$pkgver.tar.gz
	plantuml.run
	"

# secfixes
#   1.2023.10:
#     - CVE-2023-3431
#     - CVE-2023-3432

build() {
	# exclude lesser functional versions with different license
	sed -e "/include/d" -i settings.gradle.kts
	export GRADLE_USER_HOME="$srcdir"/.gradle
	./gradlew --no-daemon --parallel --stacktrace assemble
}

check() {
	# gradle test (will be seperate call with gradle v9)
	./gradlew --no-daemon --parallel --stacktrace check
}

package() {
	install -Dm644 ./build/libs/plantuml-$pkgver.jar \
		"$pkgdir/usr/share/java/plantuml.jar"
	install -Dm755 "$srcdir/plantuml.run" \
		"$pkgdir/usr/bin/plantuml"
}

sha512sums="
cb51e44691eb0a206de3f23e0bf1ba08f8a9d111d838c8b5f2ef2e7b7bae61294deb935bd0c0f2798fdc51d7a0a3e54d40d201075964e109a7f0781010933e91  plantuml-1.2024.4.tar.gz
4b2be5783dffd9aeb4e49c4c192f24e182cc55e39dae45a34f7cf42a0174c22aa0ada60230d6714e473f316ec230d0abec09cfdbeae27e3de0f26861a9814f8e  plantuml.run
"
